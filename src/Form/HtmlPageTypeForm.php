<?php

namespace Drupal\html_page\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HtmlPageTypeForm.
 */
class HtmlPageTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $html_page_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $html_page_type->label(),
      '#description' => $this->t("Label for the Html page type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $html_page_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\html_page\Entity\HtmlPageType::load',
      ],
      '#disabled' => !$html_page_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $html_page_type = $this->entity;
    $status = $html_page_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Html page type.', [
          '%label' => $html_page_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Html page type.', [
          '%label' => $html_page_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($html_page_type->toUrl('collection'));
  }

}
