<?php

namespace Drupal\html_page\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Html page entities.
 *
 * @ingroup html_page
 */
class HtmlPageDeleteForm extends ContentEntityDeleteForm {


}
