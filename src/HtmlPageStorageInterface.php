<?php

namespace Drupal\html_page;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\html_page\Entity\HtmlPageInterface;

/**
 * Defines the storage handler class for Html page entities.
 *
 * This extends the base storage class, adding required special handling for
 * Html page entities.
 *
 * @ingroup html_page
 */
interface HtmlPageStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Html page revision IDs for a specific Html page.
   *
   * @param \Drupal\html_page\Entity\HtmlPageInterface $entity
   *   The Html page entity.
   *
   * @return int[]
   *   Html page revision IDs (in ascending order).
   */
  public function revisionIds(HtmlPageInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Html page author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Html page revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\html_page\Entity\HtmlPageInterface $entity
   *   The Html page entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(HtmlPageInterface $entity);

  /**
   * Unsets the language for all Html page with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
