<?php

namespace Drupal\html_page;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for html_page.
 */
class HtmlPageTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
