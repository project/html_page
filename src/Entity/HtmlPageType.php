<?php

namespace Drupal\html_page\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Html page type entity.
 *
 * @ConfigEntityType(
 *   id = "html_page_type",
 *   label = @Translation("Html page type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\html_page\HtmlPageTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\html_page\Form\HtmlPageTypeForm",
 *       "edit" = "Drupal\html_page\Form\HtmlPageTypeForm",
 *       "delete" = "Drupal\html_page\Form\HtmlPageTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\html_page\HtmlPageTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "html_page_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "html_page",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/html_page_type/{html_page_type}",
 *     "add-form" = "/admin/structure/html_page_type/add",
 *     "edit-form" = "/admin/structure/html_page_type/{html_page_type}/edit",
 *     "delete-form" = "/admin/structure/html_page_type/{html_page_type}/delete",
 *     "collection" = "/admin/structure/html_page_type"
 *   }
 * )
 */
class HtmlPageType extends ConfigEntityBundleBase implements HtmlPageTypeInterface {

  /**
   * The Html page type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Html page type label.
   *
   * @var string
   */
  protected $label;

}
