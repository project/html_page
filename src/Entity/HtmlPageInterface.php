<?php

namespace Drupal\html_page\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Html page entities.
 *
 * @ingroup html_page
 */
interface HtmlPageInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Html page name.
   *
   * @return string
   *   Name of the Html page.
   */
  public function getName();

  /**
   * Sets the Html page name.
   *
   * @param string $name
   *   The Html page name.
   *
   * @return \Drupal\html_page\Entity\HtmlPageInterface
   *   The called Html page entity.
   */
  public function setName($name);

  /**
   * Gets the Html page creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Html page.
   */
  public function getCreatedTime();

  /**
   * Sets the Html page creation timestamp.
   *
   * @param int $timestamp
   *   The Html page creation timestamp.
   *
   * @return \Drupal\html_page\Entity\HtmlPageInterface
   *   The called Html page entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Html page revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Html page revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\html_page\Entity\HtmlPageInterface
   *   The called Html page entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Html page revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Html page revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\html_page\Entity\HtmlPageInterface
   *   The called Html page entity.
   */
  public function setRevisionUserId($uid);

}
