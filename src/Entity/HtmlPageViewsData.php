<?php

namespace Drupal\html_page\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Html page entities.
 */
class HtmlPageViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
