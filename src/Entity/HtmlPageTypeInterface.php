<?php

namespace Drupal\html_page\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Html page type entities.
 */
interface HtmlPageTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
