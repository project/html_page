<?php

namespace Drupal\html_page;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\html_page\Entity\HtmlPageInterface;

/**
 * Defines the storage handler class for Html page entities.
 *
 * This extends the base storage class, adding required special handling for
 * Html page entities.
 *
 * @ingroup html_page
 */
class HtmlPageStorage extends SqlContentEntityStorage implements HtmlPageStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(HtmlPageInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {html_page_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {html_page_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(HtmlPageInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {html_page_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('html_page_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
