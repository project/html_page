<?php

namespace Drupal\html_page\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\html_page\Entity\HtmlPageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HtmlPageController.
 *
 *  Returns responses for Html page routes.
 */
class HtmlPageController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Html page revision.
   *
   * @param int $html_page_revision
   *   The Html page revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($html_page_revision) {
    $html_page = $this->entityTypeManager()->getStorage('html_page')
      ->loadRevision($html_page_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('html_page');

    return $view_builder->view($html_page);
  }

  /**
   * Page title callback for a Html page revision.
   *
   * @param int $html_page_revision
   *   The Html page revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($html_page_revision) {
    $html_page = $this->entityTypeManager()->getStorage('html_page')
      ->loadRevision($html_page_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $html_page->label(),
      '%date' => $this->dateFormatter->format($html_page->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Html page.
   *
   * @param \Drupal\html_page\Entity\HtmlPageInterface $html_page
   *   A Html page object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(HtmlPageInterface $html_page) {
    $account = $this->currentUser();
    $html_page_storage = $this->entityTypeManager()->getStorage('html_page');

    $langcode = $html_page->language()->getId();
    $langname = $html_page->language()->getName();
    $languages = $html_page->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $html_page->label()]) : $this->t('Revisions for %title', ['%title' => $html_page->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all html page revisions") || $account->hasPermission('administer html page entities')));
    $delete_permission = (($account->hasPermission("delete all html page revisions") || $account->hasPermission('administer html page entities')));

    $rows = [];

    $vids = $html_page_storage->revisionIds($html_page);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\html_page\HtmlPageInterface $revision */
      $revision = $html_page_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $html_page->getRevisionId()) {
          $link = $this->l($date, new Url('entity.html_page.revision', [
            'html_page' => $html_page->id(),
            'html_page_revision' => $vid,
          ]));
        }
        else {
          $link = $html_page->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.html_page.translation_revert', [
                'html_page' => $html_page->id(),
                'html_page_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.html_page.revision_revert', [
                'html_page' => $html_page->id(),
                'html_page_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.html_page.revision_delete', [
                'html_page' => $html_page->id(),
                'html_page_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['html_page_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
