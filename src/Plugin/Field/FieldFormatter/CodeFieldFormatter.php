<?php

namespace Drupal\html_page\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'code_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "code_field_formatter",
 *   label = @Translation("Code field formatter"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class CodeFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'token_replace' => 0,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['token_replace'] = [
      '#type'          => 'checkbox',
      '#description'   => $this->t('Replace text pattern. e.g %node-title-token or %node-author-name-token, by token values.', [
          '%node-title-token'       => '[node:title]',
          '%node-author-name-token' => '[node:author:name]',
        ]) . ' ' /*. $token_link*/,
      '#title'         => $this->t('Token Replace'),
      '#default_value' => $this->getSetting('token_replace'),
    ];
    return $element + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $yes     = $this->t('Yes');
    $no      = $this->t('No');
    $summary[] = $this->t('Token Replace: @token', ['@token' => $this->getSetting('token_replace') ? $yes : $no]);
    if (!\Drupal::moduleHandler()->moduleExists('token')) {
      $summary[] = $this->t('Enable token module to see available tokens to use.');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // @todo Add tokens for HTML Page entity.
    $token_data = [
      'user' => \Drupal::currentUser(),
    ];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'processed_text',
        '#text' => \Drupal::token()->replace($item->value, $token_data),
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      ];
    }

    return $elements;
  }


}
