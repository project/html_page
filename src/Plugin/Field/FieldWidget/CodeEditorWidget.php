<?php

namespace Drupal\html_page\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Plugin implementation of the 'code_editor_widget' widget.
 *
 * @FieldWidget(
 *   id = "code_editor_widget",
 *   module = "html_page",
 *   label = @Translation("Code editor widget"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class CodeEditorWidget extends TextareaWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'language' => 'html',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['language'] = [
      '#type' => 'select',
      '#options' => [
        'html' => $this->t('HTML'),
        'css' => $this->t('CSS'),
        'js' => $this->t('JavaScript'),
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Language: @lang', ['@lang' => $this->getSetting('language')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#format'] = 'pure_html';
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $element['tokens'] =  \Drupal::service('token.tree_builder')->buildRenderable(['user']);
      $element['tokens']['#suffix'] = '<strong>*' . $this->t('Since this is a code editor and not your usual textarea field,
        you will have to copy and paste the required tokens, unlike your usual click and it will appear in the textarea.') . '</strong>';
    }
    else {
      $element['#prefix'] = '<strong>*Enabling the token module will allow you to see the available tokens that can be used here.</strong>';
    }
    return $element;
  }

}
