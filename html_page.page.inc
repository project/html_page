<?php

/**
 * @file
 * Contains html_page.page.inc.
 *
 * Page callback for Html page entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Html page templates.
 *
 * Default template: html_page.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_html_page(array &$variables) {
  // Fetch HtmlPage Entity Object.
  $html_page = $variables['elements']['#html_page'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
