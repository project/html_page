# Introduction

The HTML Page module allows webmasters to build custom landing pages using Ace editor. This module provides an entity to design a complete page in HTML that stands apart from other pages, mostly as a landing page of sorts. Ace editor provides excellent syntax highlighting and you can add your HTML, CSS, JS right in there.

This module allows you to directly add HTML of pre-built templates to design a full HTML page outside of Drupal, while also not interfering with the default theme of your Drupal application.

There are two bundles provided for the entity **HTML Page**, viz., **Independent HTML Page** and **HTML Page with Drupal regions**.

Independent HTML Page allows you to create a full page independent of your existing theme. The HTML, CSS and if required JavaScript should also be provided by you. There will be no regions or styles provided by the theme, everything rendered on the page will be from the HTML you added.

HTML Page with Drupal regions is like any other content. You add the HTML content and it will be rendered in the content region of the theme.

The permission to create HTML Page entities should only be given to trusted users and one must be careful of the JavaScript files included in the HTML.

# Features
1. Write purely HTML pages without worrying about your theme styles.
2. Ace editor for excellent syntax highlighting.
3. The code editor supports token replacement. If you enable token module you can see the available tokens.

# How to install and use

Using composer to install this is the best way. Although you can directly download and install it, it is not recommended because of external dependencies.

If you are using composer, there are a few things to keep in mind. 

1. Add the following to the repositories section in your composer.json file.
```json
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
```
2. Add the following external library in your repositories section, so that you can download the libraries required for **Ace Editor**
```json
    {
        "type": "package",
        "package": {
            "name": "ajaxorg/ace-builds",
            "version": "1.4.12",
            "type": "drupal-library",
            "dist": {
                "url": "https://github.com/ajaxorg/ace-builds/archive/v1.4.12.zip",
                "type": "zip"
            }
        }
    }
```
3. After the previous step run `composer require ajaxorg/ace-builds` so that it is downloaded into the libraries folder.
4. Once all previous steps are successful, run `composer require drupal/html_page`. This will install the module and it's dependency **Ace Editor**.
5. Enable the module and you will have a new entity **HTML Page** which has two bundles for you to add pure HTML content; viz., **Independent HTML Page** and **HTML Page with Drupal regions**.
 Independent HTML Page allows you to create a full page independent of your existing theme. The HTML, CSS and if required JavaScript should also be provided by you. There will be no regions or styles provided by the theme, everything rendered on the page will be from the HTML you added.HTML Page with Drupal regions is like any other content. You add the HTML content and it will be rendered in the content region of the theme.
 6. To add new HTML Page, go to '`/html-page/html_page`'. You can also get here by clicking on Structure from the Admin toolbar.

 # Requirements
 1. Drupal 8/9
 2. Ace Editor Drupal module and it's dependencies
 3. Composer
 4. Token module (Optional)
 